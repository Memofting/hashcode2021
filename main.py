from solution import Solution
from statement import Statement


def main():
    statement = Statement()
    solution = Solution.read(statement)
    # solution = Solution([
    #     [Light('rue-de-londres', 1)],
    #     [Light('rue-d-amsterdam', 1), Light('rue-d-athenes', 1)],
    #     [Light('rue-de-rome', 1)],
    #     [Light('rue-de-moscou', 1)]
    # ])
    print(solution.simulate(statement))


if __name__ == '__main__':
    main()
