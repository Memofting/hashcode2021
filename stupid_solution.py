from collections import defaultdict


class Street:
    def __init__(self):
        self.start_intersection = 0
        self.end_intersection = 0
        self.name = ""
        self.length = 0


def main():
    duration, intesections, streets_count, cars_count, points = map(int, input().split())

    streets = list()
    name_to_street = dict()
    intersection_to_streets = defaultdict(list)

    for _ in range(streets_count):
        start_intersection, end_intersection, name, length = input().split()
        s = Street()
        s.start_intersection = int(start_intersection)
        s.end_intersection = int(end_intersection)
        s.name = name
        s.length = int(length)

        streets.append(s)
        name_to_street[name] = s
        intersection_to_streets[s.end_intersection].append(s)

    streets_name_count = defaultdict(int)
    for _ in range(cars_count):
        streets_for_car = input().split()[1:]
        for street_for_car in streets_for_car:
            streets_name_count[street_for_car] += 1

    cycle_length = 20
    count_barrier = -1

    buffer_dict = defaultdict(list)
    street_to_seconds = dict()
    for i in intersection_to_streets.keys():
        i_streets = intersection_to_streets[i]

        used_streets = list()
        used_count = 0
        for cur_i_str in i_streets:
            if cur_i_str.name in streets_name_count.keys():
                s_n_count = streets_name_count[cur_i_str.name]
                if s_n_count <= count_barrier:
                    continue
                used_streets.append(cur_i_str)
                used_count += s_n_count

        if len(used_streets) != 0:
            buffer_dict[i] = used_streets
            min_seconds = 99999999999999999
            for used_street in used_streets:
                street_to_seconds[used_street.name] = 1 + int(
                    (1.0 * streets_name_count[used_street.name] / used_count) * cycle_length)
                if street_to_seconds[used_street.name] < min_seconds:
                    min_seconds = street_to_seconds[used_street.name]
            min_seconds -= 1
            for used_street in used_streets:
                street_to_seconds[used_street.name] -= min_seconds
                if street_to_seconds[used_street.name] >= 10:
                    street_to_seconds[used_street.name] = 10

    print(len(buffer_dict))
    for i in buffer_dict.keys():
        print(i)
        print(len(buffer_dict[i]))
        for i_s in buffer_dict[i]:
            print('{0} {1}'.format(i_s.name, street_to_seconds[i_s.name]))


main()