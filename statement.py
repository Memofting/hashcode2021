from dataclasses import dataclass
import typing as t


@dataclass
class Street:
    start_intersection: int
    end_intersection: int
    name: str
    length: int


@dataclass
class Car:
    street_names: t.List[str]


class Statement:
    duration: int
    intersection: int
    number_streets: int
    number_cars: int
    bonus: int

    streets: t.List[Street]
    cars: t.List[Car]
    rev_streets: t.Dict[str, Street]

    def __init__(self):
        self.duration, self.intersection, self.number_streets, self.number_cars, self.bonus = map(int, input().split())
        self.streets = []
        self.cars = []
        self.rev_streets = {}

        for _ in range(self.number_streets):
            start_intersection, end_intersection, name, length = input().split()
            self.streets.append(Street(int(start_intersection), int(end_intersection), name, int(length)))
            self.rev_streets[name] = self.streets[-1]

        for _ in range(self.number_cars):
            self.cars.append(Car(input().split()[1:]))
