import typing as t
from collections import deque
from copy import deepcopy
from dataclasses import dataclass

from statement import Statement


@dataclass
class Light:
    street: str
    duration: int


class Solution:
    lights: t.List[t.List[Light]]

    def __init__(self, lights: t.List[t.List[Light]]):
        self.lights = lights

    @staticmethod
    def read(statement: Statement) -> "Solution":
        n = int(input())
        lights = [[] for _ in range(statement.number_streets)]
        for _ in range(n):
            intersection = int(input())
            n_lights = int(input())
            for __ in range(n_lights):
                street, duration = input().split()
                duration = int(duration)
                lights[intersection].append(Light(street, duration))
        return Solution(lights)

    def simulate(self, statement: Statement) -> int:
        gain: int = 0

        cars = [
            [(street, statement.rev_streets[street].length) for street in car.street_names][::-1]
            for car in statement.cars
        ]
        lights = [
            [(light.street, light.duration) for light in lights][::-1]
            for intersection, lights in enumerate(self.lights)
        ]
        start_lights = deepcopy(lights)
        street_light: t.Dict[str, bool] = {street.name: False for street in statement.streets}
        street_passed: t.Dict[str, t.Deque[int]] = {street.name: deque() for street in statement.streets}

        for time in range(0, statement.duration):
            for intersection in range(len(lights)):
                if not start_lights[intersection]:
                    continue
                lights_in_intersection = lights[intersection]
                street, duration = lights_in_intersection[-1]
                if duration == 0:
                    lights_in_intersection.pop()
                    street_light[street] = False
                    if not lights[intersection]:
                        lights_in_intersection = lights[intersection] = deepcopy(start_lights[intersection])
                    street, duration = lights_in_intersection[-1]
                duration -= 1
                street_light[street] = True
                lights_in_intersection[-1] = street, duration

            for street, passed in street_passed.items():
                if not passed:
                    continue
                car = passed.popleft()
                if street_light[street]:
                    cars[car].pop()
                    if not cars[car]:
                        gain += statement.bonus + (statement.duration - time)

            for car, car_path in enumerate(cars):
                if not car_path:
                    continue
                street, duration = car_path[-1]
                if duration != 0:
                    duration -= 1
                else:
                    if car not in street_passed[street]:
                        street_passed[street].append(car)
                car_path[-1] = street, duration
            # print(time)
        return gain

    def __str__(self) -> str:
        ans = ""
        a = sum([1 for lights in self.lights if lights])
        ans += f"{a}\n"
        for intersection, lights in enumerate(self.lights):
            if not lights:
                continue
            ans += f"{intersection}\n"
            ans += f"{len(lights)}\n"
            for street, duration in lights:
                ans += f"{street} {duration}\n"
        return ans
